import pygame
from gtts import gTTS
from threading import *
from selenium import webdriver
import speech_recognition as sr
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=800x600")
chrome_options.add_argument('--disable-gpu')
chromedriver = 'C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe'
chrome_options.add_argument("--user-data-dir=C:\\Users\\meren\\AppData\\Local\\Google\\Chrome\\User Data")

def whatsapp(recipient, message):
    driver = webdriver.Chrome(chromedriver, chrome_options=chrome_options)
    driver.get("https://web.whatsapp.com/")
    print("Invio in corso...")
    tts = gTTS("Invio in corso", lang="it")
    tts.save("sending.mp3")
    pygame.mixer.music.load("sending.mp3")
    pygame.mixer.music.play()

    try:
        WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.XPATH, "//span[@title='" + recipient + "']")))
        print ("Page is ready!")
    except TimeoutException:
        print ("Loading took too much time!")

    user = driver.find_element_by_xpath("//span[@title='" + recipient + "']")
    user.click()
    input_field = driver.find_element_by_class_name("_1Plpp")
    input_field.send_keys(message)
    button = driver.find_element_by_class_name("_35EW6")
    button.click()
    tts = gTTS("Messaggio inviato", lang="it")
    tts.save("message_sent.mp3")
    pygame.mixer.music.load("message_sent.mp3")
    pygame.mixer.music.play()

def custom_train():
    with open("conversazione.txt") as f:
        conversation = f.readlines()
        bot.set_trainer(ListTrainer)
        bot.train(conversation)
        f.close()

def google_train():
    with open("internet_search.txt", encoding='UTF8') as f:
        statements = f.readlines()
        bot.set_trainer(ListTrainer)
        bot.train(statements)
        f.write("")
        f.close()

def corpus_train():
    bot.set_trainer(ChatterBotCorpusTrainer)
    bot.train("chatterbot.corpus.italian")
    bot.logger.info('Trained database generaUltron successfully!')
