﻿# -*- coding: utf-8 -*-
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from chatterbot.trainers import ChatterBotCorpusTrainer
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import speech_recognition as sr
from gtts import gTTS
import vlc
import codecs
import sys
import time
import os
import re
import pygame
import winsound
from threading import *
import pafy
from functions import *
import keyboard

rindex = 1
n = 2
user_input = ""

bot = ChatBot(
    "Ultron",
    read_only=False,
    storage_adapter="chatterbot.storage.SQLStorageAdapter",
    logic_adapters=["chatterbot.logic.LowConfidenceAdapter",
                    "chatterbot.logic.BestMatch"],
    filters=["chatterbot.filters.RepetitiveResponseFilter"],
    threshold=0.65,
    default_response="unknown_response",
    input_adapter="chatterbot.input.VariableInputTypeAdapter",
    output_adapter="chatterbot.output.OutputAdapter",
    trainer="chatterbot.trainers.ListTrainer"
)

start = True

class InputThread (Thread):
   def __init__(self):
      Thread.__init__(self)
   def run(self):
       global user_input
       global start
       while True:
           if start == True:
               r = sr.Recognizer()
               with sr.Microphone() as source:
                   r.adjust_for_ambient_noise(source)
                   audio = r.listen(source)
                   try:
                       user_input = r.recognize_google(audio, language="it_IT")
                       print(">>> ", user_input)
                       if keyboard.is_pressed(''):
                           print('You Pressed A Key!')
                   except:
                       continue

class MusicThread (Thread):
   def __init__(self):
      Thread.__init__(self)
   def run(self):
       driver = webdriver.Chrome(chromedriver, chrome_options = chrome_options)
       driver.get("https://www.youtube.com/results?search_query=" + user_input) #/watch?v=LDU_Txk06tM
       url = driver.find_element_by_id("thumbnail").get_attribute("href")
       print (url)
       video = pafy.new(url)
       best = video.getbest()
       playurl = best.url
       Instance = vlc.Instance()
       player = Instance.media_player_new()
       Media = Instance.media_new(playurl)
       Media.get_mrl()
       player.set_media(Media)
       while True:
           if(player.get_state() != "playing"):
               player.play()
           if "stop" in user_input:
               player.stop()
               break
       sys.exit(1)
       exec(tName + "._stop()")
       exec(tName + ".join()")

os.system("TASKKILL /F /IM chrome.exe")

input_thread = InputThread()
input_thread.daemon = True
input_thread.start()

print("Ultron: Ciao! è bello essere vivi!")
pygame.mixer.init(frequency=24500, size=16, channels=2, buffer=4096)
pygame.mixer.music.load("voice_output_start.mp3")
pygame.mixer.music.play()

while True:
    if "Ultron" in user_input:
        user_input = re.sub(r'.*Ultron', '', user_input)

        if user_input == "":
            continue

        if "spegnimento" in user_input:
            print("Ultron: Arrivederci!")
            tts = gTTS("Spegnimento in, 3, 2, 1", lang="it")
            tts.save("voice_output_goodbye.mp3")
            pygame.mixer.music.load("voice_output_goodbye.mp3")
            pygame.mixer.music.play()
            while True:
                if(pygame.mixer.music.get_busy() == False):
                    break
            sys.exit(1)
            os._exit(1)

        if "Invia un messaggio" in user_input:
            os.system("TASKKILL /F /IM chrome.exe")
            tts = gTTS("Chi è il destinatario?", lang="it")
            tts.save("recipient_question.mp3")
            pygame.mixer.music.load("recipient_question.mp3")
            start = False
            pygame.mixer.music.play()
            time.sleep(2.5)
            start = True
            user_input = ""
            while True:
                if user_input != "":
                    user_input = re.sub(r'.*Chi è il destinatario', '', user_input)
                    recipient = user_input
                    if(recipient != ""):
                        print("Destinatario: ", recipient.title())
                        break
                    else:
                        continue

            tts = gTTS("Cosa vuoi che dica?", lang="it")
            tts.save("message_question.mp3")
            pygame.mixer.music.load("message_question.mp3")
            start = False
            pygame.mixer.music.play()
            time.sleep(1.5)
            start = True
            user_input = ""
            while True:
                if user_input != "":
                    user_input = re.sub(r'.*Cosa vuoi che dica', '', user_input)
                    message = user_input
                    if(message != ""):
                        print("Messaggio: ", message)
                        break
                    else:
                        continue

            whatsapp(recipient.title(), message)
            user_input = ""
            continue

        if "Riproduci" in user_input:
            os.system("TASKKILL /F /IM chrome.exe")
            tName = "thread" + str(n)
            exec(tName + " = MusicThread()")
            user_input = re.sub(r'.*riproduci', '', user_input)
            user_input = user_input.replace(" ", "+")
            exec(tName + ".daemon = True")
            exec(tName + ".start()")
            n = n + 1

        else:
            bot.read_only=True

            response = bot.get_response(user_input)
            print("Ultron: ", response)

            if(response == "unknown_response"):
                os.system("TASKKILL /F /IM chrome.exe")
                pygame.mixer.init(frequency=22050, size=16, channels=2, buffer=4096)
                pygame.mixer.music.load("thinking_sound.mp3")
                pygame.mixer.music.play()
                try:
                    #search_results = google.search(user_input, 1)
                    #print(search_results)
                    #query = wikipedia.summary(user_input, sentences = 1)

                    driver = webdriver.Chrome(chromedriver, chrome_options = chrome_options)
                    driver.get("https://www.google.it")
                    assert "Google" in driver.title
                    google_search = driver.find_element_by_name("q")
                    google_search.clear()
                    google_search.send_keys(user_input)
                    google_search.send_keys(Keys.RETURN)

                    list=[]

                    results = driver.find_elements_by_xpath("//span[@class='ILfuVd']") #risultati siti web
                    isPresent = len(results) > 0
                    if(isPresent):
                            for i in results:
                                list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='Z0LcW']") #feedback google
                    isPresentFeedback = len(results) > 0
                    if(isPresentFeedback):
                            for i in results:
                                list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='IAznY']/div[@class='title']") #lista google
                    isPresent = len(results) > 0
                    if(isPresent):
                            for i in results:
                                list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='kltat']") #attori
                    isPresentActor = len(results) > 0
                    if(isPresentActor):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='hb8SAc kno-fb-ctx']/div/div/div/span") #personaggio banner
                    isPresent = len(results) > 0
                    if(isPresent and isPresentActor == False and isPresentFeedback == False):
                        for i in range(len(results) - 1):
                            list.append(results[i].text)

                    results = driver.find_elements_by_xpath("//span[@class='cwcot gsrt']") #calcolatrice
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='vk_bk dDoNo']") #orario
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='BbbuR uc9Qxb uE1RRc']") #maps
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in range(1):
                            list.append(results[i].text)

                    results = driver.find_elements_by_xpath("//div[@class='vk_bk sol-tmp']/span[1]") #meteo gradi
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text + " gradi centigradi")

                    results = driver.find_elements_by_xpath("//div[@class='vk_gy vk_sh wob-dtl']/div") #meteo statistiche
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in range(len(results) - 1):
                            list.append(results[i].text)

                    results = driver.find_elements_by_xpath("//div[@class='MUxGbd t51gnb lyLwlc lEBKkf']") #orario alba
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//span[@class='DFlfde']") #conversione valuta
                    isPresent = len(results) > 0
                    if(isPresent):
                        list.append(results[1].text)

                    results = driver.find_elements_by_xpath("//div[@class='PNlCoe XpoqFe']/div/span") #vocabolario
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='kpd-ans kno-fb-ctx KBXm4e']") #popolazione
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='kno-rdesc r-iTBnrynTFErY']/div/h3[@class='bNg8Rb']/span") #luogo banner RqBzHd
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    results = driver.find_elements_by_xpath("//div[@class='RqBzHd']") #ingredienti
                    isPresent = len(results) > 0
                    if(isPresent):
                        for i in results:
                            list.append(i.text)

                    print(list)

                    if not list:
                        print("Mi spiace, non ho trovato nessun risultato.")
                        tts = gTTS("Mi spiace, non ho trovato nessun risultato.", lang="it")
                        tts.save("voice_output_error.mp3")
                        pygame.mixer.music.stop()
                        pygame.mixer.init(frequency=24500, size=16, channels=2, buffer=4096)
                        pygame.mixer.music.load("voice_output_error.mp3")
                        pygame.mixer.music.play()
                        continue

                    index = 1
                    for i in list:
                        if(i == ""):
                            continue
                        tts = gTTS(i, lang="it")
                        tts.save("voice_output_search_"+str(index).rjust(3, '0')+".mp3")
                        pygame.mixer.music.stop()
                        pygame.mixer.init(frequency=24500, size=16, channels=2, buffer=4096)
                        pygame.mixer.music.load("voice_output_search_"+str(index).rjust(3, '0')+".mp3")
                        pygame.mixer.music.play()
                        while True:
                            if(pygame.mixer.music.get_busy() == False):
                                break
                        index = index + 1

                except(KeyboardInterrupt, EOFError, SystemExit):
                    continue

            else:
                pygame.quit()
                tts = gTTS(response.text, lang="it")
                tts.save("voice_output_response_"+str(rindex).rjust(3, '0')+".mp3")
                pygame.mixer.init(frequency=24500, size=16, channels=2, buffer=4096)
                pygame.mixer.music.load("voice_output_response_"+str(rindex).rjust(3, '0')+".mp3")
                pygame.mixer.music.play()
                try:
                    os.remove("voice_output_response_"+str(rindex - 1).rjust(3, '0')+".mp3")
                except:
                    print("not exist")
                rindex = rindex + 1
